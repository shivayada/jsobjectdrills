const  pairs = require("../pairs.cjs");
const  testObject = require('../data.cjs');

test ("provide the new object" ,() =>{
    expect(pairs(testObject)).toStrictEqual([ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ])
})