const  invert = require("../invert.cjs");
const  testObject = require('../data.cjs');

test ("provide the new object" ,() =>{
    expect(invert(testObject)).toStrictEqual({ '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' })
})