const  mapObject = require("../mapobject.cjs");
const  testObject = require('../data.cjs');

test ("provide the new object" ,() =>{
    expect(mapObject(testObject)).toStrictEqual({ name: 'Bruce Wayne', age: 37, location: 'Gotham' })
})