const  keys = require("../keys.cjs");
const  testObject = require('../data.cjs');

test ("provide the new object" ,() =>{
    expect(keys(testObject)).toStrictEqual([ 'name', 'age', 'location' ])
})