const testObject= require('./data.cjs');


function pairs(obj){
    let newArray=[];
    for(let item in obj){
        let sub=[];
        sub.push(item);
        sub.push(obj[item]);
        newArray.push(sub);
    }
    return newArray;
}

console.log(pairs(testObject));

module.exports = pairs;