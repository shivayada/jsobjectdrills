const testObject= require('./data.cjs');


function invert(obj){
    let newobj={};
    for(let item in obj){
       newobj[obj[item]]= item;
    }
    return newobj;
}

console.log(invert(testObject));

module.exports =invert;