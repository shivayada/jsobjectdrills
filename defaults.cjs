const testObject= require('./data.cjs');


function defaults(obj, defaultProps){ 
    for( let  item in defaultProps){
        if(!(item in obj)){
            obj[item]= defaultProps[item];
        }
    }
    return obj;

}

console.log(defaults(testObject, { age:37 , weight : 59}));

module.exports = defaults;