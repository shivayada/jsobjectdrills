
const testObject= require('./data.cjs');

function cb( value, key){
    if(typeof value=== 'number'){
        return value+1;
    }
    return value;
}

function mapObject(obj) {
    let keyitems= [];
    for(let item in obj){
        obj[item]= cb( obj[item], item);
    }
    return obj;
}



module.exports = mapObject;